import pulp
from tools import computation


def write_online(list_G, vectors_encoutered, number_attackers, loss_sum, optimal_sol, filename):
    m = len(list_G[0])  # Number of types of attackers
    f = open(filename, 'w')
    f.write('List strategies \n')
    for G in list_G:
        f.write(str(G))
        f.write('\n')
    f.write('Vectors encountered \n')
    f.write(str(vectors_encoutered))
    f.write('\n')
    f.write('number_attackers \n')
    f.write(str(number_attackers))
    f.write('\n')
    f.write('Average total loss \n')
    f.write(str(loss_sum))
    f.write('\n')
    Geql = computation.retrieve_gain_eql(optimal_sol, m)
    optimal_loss = pulp.value(optimal_sol.objective)
    f.write('Optimal strategy \n')
    f.write(str(Geql))
    f.write('\n')
    f.write('Optimal loss \n')
    f.write(str(optimal_loss))
    f.close()


def read_list(file, is_int):
    line = file.readline()
    line = line.rstrip()
    list_str = line.strip('][').split(', ')
    if(is_int):
        list_elt = [int(list_str[i]) for i in range(len(list_str))]
    else:
        list_elt = [float(list_str[i]) for i in range(len(list_str))]
    return list_elt


def read_online(filename, T):
    file = open(filename, 'r')
    file.readline()
    list_G = []
    for i in range(T):
        list_G.append(read_list(file, False))
    file.readline()
    vectors_encountered = read_list(file, True)
    file.readline()
    number_attackers = read_list(file, True)
    file.readline()
    loss_sum = float(file.readline())
    # Skips optimal strategy online
    file.readline()
    file.readline()
    # Reads best possible loss
    file.readline()
    optimal_sol = float(file.readline())
    file.close()
    return list_G, vectors_encountered, number_attackers, loss_sum, optimal_sol


def write_eql(Geql, objective, computation_time, filename):
    m = len(Geql)
    file = open(filename, 'w')
    file.write("strategy \n")
    for j in range(m):
        file.write(str(Geql[j]))
        file.write("\n")
    file.write("objective \n")
    file.write(str(objective))
    file.write("\n")
    file.write("computation time \n")
    file.write(str(computation_time))
    file.write('\n')
    file.close()


def write_training(gain_vector, computation_time, objective_reduced, filename):
    file = open(filename, 'w')
    file.write('Strategy \n')
    file.write(str(gain_vector))
    file.write('\n')
    file.write('Computation time \n')
    file.write(str(computation_time))
    file.write('\n')
    file.write('Minimum gain \n')
    file.write(str(objective_reduced))
    file.write('\n')
    file.close()


def read_training(name):
    filename = name
    file = open(filename, 'r')
    file.readline()
    strategy = read_list(file, False)
    file.readline()
    computation_time = float(file.readline())
    file.readline()
    min_gain = float(file.readline())
    file.close()
    return strategy, computation_time, min_gain


def read_eql(name, m):
    Geql = []
    file = open(name, 'r')
    file.readline()
    for i in range(m):
        Geql.append(float(file.readline()))
    file.readline()
    objective = float(file.readline())
    file.readline()
    time = float(file.readline())
    file.close()
    return Geql, objective, time


def read_eql_2(name, m):
    file = open(name, 'r')
    file.readline()
    Geql = read_list(file, False)
    file.readline()
    objective = float(file.readline())
    file.readline()
    time = float(file.readline())
    file.close()
    return Geql, objective, time
