import math
import numpy as np

def project(gain, Gmin, Gmax):
    """Projects the parameter G on the space of feasible parameters"""
    for i in range(len(gain)):
        if(gain[i] > Gmax[i]):
            gain[i] = Gmax[i]
        elif(gain[i] < Gmin[i]):
            gain[i] = Gmin[i]


def update_G(G, gradient, eta, Gmin, Gmax):
    """Performs the update space of a gradient method"""
    for j in range(len(G)):
        G[j] -= eta * gradient[j]
    project(G, Gmin, Gmax)


def update_parameters_normal_user(gain_attacker, cfa, gain_success, loss_failure, Gmin, Gmax, t):
    """
    - gain_attacker: the current strategy
    - cfa: The false alarm cost
    - gain_success: gain of an attacker in case of successfull attack
    - loss_failure: loss of an attacker in case of unsucessfull attack
    - t the current step
    """
    eta = 1 / (math.sqrt(t))
    gradient = gradient_normal_user(
        gain_attacker, cfa, gain_success, loss_failure)
    new_gain = gain_attacker
    for i in range(len(gain_attacker)):
        new_gain[i] = new_gain[i] - eta * gradient[i]
        # Projects on [Gmin, Gmax]
        if(new_gain[i] > Gmax[i]):
            new_gain[i] = Gmax[i]
        elif(new_gain[i] < Gmin[i]):
            new_gain[i] = Gmin[i]
    return new_gain


def gradient_attack(gain_attacker, type_attacker):
    """
    Returns the gradient of the loss of the defender facing an attacker where:
    - gain_attacker is the strategy of the defender
    - type_attacker is the type of the attacker the defender faced
    In this case, l_t(G_t) = -G_type_attacker
    """
    m = len(gain_attacker)
    gradient = [0 for i in range(m)]
    gradient[type_attacker] = 1
    return gradient

def gradient_normal_user(gain_attacker, CFA, gain_success, loss_failure):
    """
    Returns the gradient of the loss of the defender where:
    - gain_attacker is the strategy of the defender
    - cfa is the false alarm cost of the vector
    - gain_success is the gain of the attacker in case of successful attack using this vector
    - loss_failure is its gain in case of failed attack.
    In this case, l_t(G_t) = pi_{G_t}(v)cfa(v)
    """
    m = len(gain_attacker)
    gradient = [0 for i in range(m)]
    max_proba = 0
    attacker_with_max = 0
    for attacker in range(m):
        if(gain_success[attacker] + loss_failure[attacker] == 0):
            proba_attacker = 0
        else:
            proba_attacker = (gain_success[attacker] - gain_attacker[attacker]
                              ) / (gain_success[attacker] + loss_failure[attacker])
        if(proba_attacker > max_proba):
            attacker_with_max = attacker
            max_proba = proba_attacker
    if(max_proba > 0):
        gradient[attacker_with_max] = -CFA / (
            gain_success[attacker_with_max] + loss_failure[attacker_with_max])
    return gradient


def distance(G1, G2):
    """ Computes the distance in norm 2 between G1 and G2
    """
    dist = 0
    for i in range(len(G1)):
        dist += (G2[i] - G1[i])**2
    dist = math.sqrt(dist)
    return dist


def max_grad(pa, proba_attacker, loss_failure, gain_success, CFA, PN, Gmin):
    M = 1
    m = len(proba_attacker)
    n = len(loss_failure)
    null_vector = np.zeros(m)
    for v in range(n):
        gradient_norm = distance(
            gradient_normal_user(
                Gmin, CFA, gain_success, loss_failure, v), null_vector)
        if(gradient_norm > M):
            M = gradient_norm
    return M
