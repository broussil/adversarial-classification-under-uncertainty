import numpy
import pulp


def compute_gain_defender(gainAttacker, probaAttacker, probaDetection, cfa, PN, p):
    """
    Computes the gain of the defender given:
    - gain_attacker the gain of each type of attacker
    - proba_attacker the probability of presence of each type of attacker
    - proba_detection the strategy of the defender
    - cfa the false alarm costs
    - PN the probability of play by non attackers
    - p the probability that a vector comes from an attacker
    """
    payoff = 0
    for j in range(len(gainAttacker)):
        payoff += probaAttacker[j] * gainAttacker[j]
    for vector in range(len(cfa)):
        payoff += cfa[vector] * probaDetection[vector] * PN[vector] * (1 - p)/p
    return payoff


def solve_defender(p, n, m, loss_failure, gain_success, PN, CFA, proba_attack):
    lp_prob = pulp.LpProblem("Adversarial_classification", pulp.LpMinimize)
    g = pulp.LpVariable.dicts("g", range(m), None, None)
    pi = pulp.LpVariable.dicts("pi", range(n), 0, 1)
    if(p > 0):
        lp_prob += pulp.LpAffineExpression([(g[j], proba_attack[j]) for j in range(m)] + [
                                           (pi[i], CFA[i] * PN[i] * (1 - p)/p) for i in range(n)])
    else:
        lp_prob += pulp.LpAffineExpression([(g[j], p*proba_attack[j]) for j in range(m)] + [
                                           (pi[i], CFA[i] * PN[i] * (1 - p)) for i in range(n)])
    for i in range(n):
        for j in range(m):
            label = "Constraint_vector_" + str(i) + "_player_" + str(j)
            condition = pulp.lpSum(
                g[j] + pi[i] * (gain_success[j][i] + loss_failure[j][i])) >= gain_success[j][i]
            lp_prob += condition, label
    lp_prob.solve()
    return lp_prob


def give_probability(gain_vector, loss_failure, gain_success):
    """
    Computes the probability of detection function pi_G given:
    - gain_vector the gain of each type of attacker
    - loss_failure the loss of the attacker in case of detected attack
    - gain_success the gain of the attacker in case of undetected attack
    """
    n = len(loss_failure[0])
    m = len(gain_vector)
    probabilities = numpy.zeros(n)
    for vector in range(n):
        maxProba = 0
        for i in range(m):
            if(gain_success[i][vector] > gain_vector[i] + 1e-5):
                # Proba of detection necessary for attaining gain Gj for vector j
                necessaryProba = (gain_success[i][vector] - gain_vector[i]) / (
                    gain_success[i][vector] + loss_failure[i][vector])
                if(necessaryProba > maxProba):
                    maxProba = necessaryProba
        probabilities[vector] = maxProba
    return probabilities


def solve_attacker(gain_vector, loss_failure, gain_success, pi, p, proba_attack, CFA, PN):
    """
    Computes a strategy of the attacker wich results in a nash equilibrium given:
    - gain_vector the gain of each type of attacker 
    - loss_failure the loss of the attacker in case of detected attack
    - gain_success the gain of the attacker in case of undetected attack
    - pi a min-max strategy of the defender
    - p the probability that a vector comes from an attacker
    - proba_attack the probability of presence of each type of attacker
    - cfa the false alarm costs
    - PN the probability of play by non-attacker
    """

    # Tolerance for the constraints
    n = len(gain_success[0])  # Number of vectors
    m = len(gain_success)  #  Number of types of attacker

    lp_prob = pulp.LpProblem("Strategy_of_attacker", pulp.LpMinimize)
    lp_prob += 0, "Arbitrary_Objective_Function"
    alpha = [[] for i in range(m)]
    for i in range(m):
        alpha[i] = pulp.LpVariable.dicts("alpha_" + str(i), range(n), 0, 1)
    # Constraints: each alpha_i must be a probability distribution
    for i in range(m):
        label = "probability_distribution_player" + str(i)
        condition = pulp.lpSum([alpha[i][v] for v in range(n)]) == 1
        lp_prob += condition, label
    # Individual constraints on alphas
    for vector in range(n):
        for i in range(m):
            # The vector is not a best-response, the attacker does not play it
            if(gain_success[i][vector] - pi[vector] * (gain_success[i][vector] + loss_failure[i][vector]) < gain_vector[i]):
                condition = pulp.lpSum(alpha[i][vector]) == 0
                label = "vector_" + str(vector) + "_not played by_" + str(i)
                lp_prob += condition, label
    for vector in range(n):
        cd_vector = (1 - p) / p * CFA[vector] * PN[vector]
        # pi(v) = 0, we upper-bound probability of pla
        if(pi[vector] == 0):
            condition = pulp.lpSum([proba_attack[i] * alpha[i][vector] * (
                gain_success[i][vector] + loss_failure[i][vector]) for i in range(m)]) <= cd_vector
            label = "lower-bound_vector_" + str(vector)
            lp_prob += condition, label
        # pi(v) = 1, we lower-bound probability of play
        if(pi[vector] == 1):
            condition = pulp.lpSum([proba_attack[i] * alpha[i][vector] * (
                gain_success[i][vector] + loss_failure[i][vector]) for i in range(m)]) >= cd_vector
            label = "upper-bound_vector_" + str(vector)
            lp_prob += condition, label

        # pi(v) in (0, 1), exact probabilty of play
        if(0 < pi[vector] < 1):
            condition = pulp.lpSum([proba_attack[i] * alpha[i][vector] * (
                gain_success[i][vector] + loss_failure[i][vector]) for i in range(m)]) == cd_vector
            label = "equality_vector_" + str(vector)
            lp_prob += condition, label
    check = lp_prob.solve()
    if(check == -1):
        print("INFEASIBLE")
    return lp_prob


def retrieve_gain_eql(defender_prob, m):
    Geql = []
    i = 0
    for v in defender_prob.variables():
        if(i < m):
            Geql.append(v.varValue)
            i += 1
    return Geql


def retrieve_attacker_strat(attacker_prob, n, m):
    alpha = [[0 for i in range(n)] for i in range(m)]
    j = 0
    vector = 0
    dummy = True
    for v in attacker_prob.variables():
        # Removes dummy variable of arbitrary objective
        if(not dummy):
            # Retrieves the number of the variable, otherwise treated by alphabetic order
            index = int(v.name[8:])
            alpha[j][index] = v.varValue
            vector += 1
            # We collected the strategy of j for every vector, we go to the following attacker
            if(vector == n):
                j += 1
                vector = 0
        dummy = False
    return alpha
