import math
import pulp
import numpy as np
import random
import copy
from tools import gradient

def distance(G1, G2):
    """ Computes the distance in norm 2 between G1 and G2
    """
    dist = 0
    for i in range(len(G1)):
        dist += (G2[i] - G1[i])**2
    dist = math.sqrt(dist)
    return dist

def pulp_solve_online(number_attackers, CFA, number_non_attackers,
                      gain_success, loss_failure):
    """
    Find the fixed minimum strategy of the online learning problem give:
    - number_attackers the number of attacker of each type encountered
    - number_non_attackers: The number of times a non-attacker played v
    """
    m = len(number_attackers)
    n = len(number_non_attackers)
    # Gain in case of success of vectors played at least once by non-attackers
    gain_success_na = [[] for j in range(m)]
    loss_failure_na = [[] for j in range(m)]
    CFA_na = []
    number_non_attackers_na = []
    number_vector = 0  # The number of different vectors encountered
    for v in range(n):
        times_played = number_non_attackers[v]
        if times_played > 0:
            number_vector += 1
            # The vector was played by a normal user
            for j in range(m):
                gain_success_na[j].append(gain_success[j][v])
                loss_failure_na[j].append(loss_failure[j][v])
            CFA_na.append(CFA[v])
            number_non_attackers_na.append(times_played)
    lp_prob = pulp.LpProblem("Online learning minimum", pulp.LpMinimize)
    g = pulp.LpVariable.dicts("g", range(m), None, None)
    pi = pulp.LpVariable.dicts("pi", range(number_vector), 0, 1)
    lp_prob += pulp.LpAffineExpression([(g[j], number_attackers[j]) for j in range(m)]
                                       + [(pi[i], CFA_na[i] * number_non_attackers_na[i]) for i in range(number_vector)])
    for i in range(number_vector):
        for j in range(m):
            label = "Constraint vector " + str(i) + " player " + str(j)
            condition = (pulp.lpSum(g[j] + pi[i] * (
                         gain_success_na[j][i] + loss_failure_na[j][i])) >= gain_success_na[j][i])
            lp_prob += condition, label
    lp_prob.solve()
    return lp_prob


def online_algorithm(pa, proba_attacker, loss_failure,
                     gain_success, CFA, PN, T, seed):
    """
    Applies the online gradient descent algorithm to the problem passed in parameters
    """
    n = len(loss_failure[0])  # Number of vectors
    m = len(proba_attacker)  # Number of attackers
    loss_sum = 0

    Gmin = [max([-loss_failure[j][v] for v in range(n)]) for j in range(m)]
    Gmax = [max(gain_success[j]) for j in range(m)]

    # Computation of online learning strategy
    np.random.seed(seed)
    random.seed(seed)
    # Keeps track of all strategies encountered
    list_G = []
    # Keeps track of how many of each type of attacker we encountered
    number_attackers = [0 for i in range(m)]
    # Keeps track of which vectors were encoutered at which time
    vectors_encountered = []
    # Initial strategy
    G = [random.uniform(Gmin[j], Gmax[j]) for j in range(m)]
    list_G.append(copy.deepcopy(G))
    # Repeats the steps of online learning
    for t in range(1, T):
        # Draws an attack or a normal vector
        rand_choice = random.uniform(0, 1)
        # We chose an attack
        if(rand_choice < pa):
            # We choose the type of the attacker attacking
            type_attacker = np.random.choice(range(m), 1, p=proba_attacker)[0]
            loss_sum += G[type_attacker]
            number_attackers[type_attacker] += 1
            grad = gradient.gradient_attack(G, type_attacker)
        # The vector will come from a normal user
        else:
            # We choose which vector comes
            v = np.random.choice(range(n), 1, p=PN)[0]
            pi_v = 0
            for j in range(m):
                proba_detect_v_j = (gain_success[j][v] - G[j]) / (
                    gain_success[j][v] + loss_failure[j][v])
                if(proba_detect_v_j > pi_v):
                    pi_v = proba_detect_v_j
            loss_sum += pi_v * CFA[v]
            vectors_encountered.append(v)
            gain_success_v = [gain_success[j][v] for j in range(m)]
            loss_failure_v = [loss_failure[j][v] for j in range(m)]
            grad = gradient.gradient_normal_user(
                G, CFA[v], gain_success_v, loss_failure_v)
        # Update gradient step
        eta = 1 / (math.sqrt(t))
        gradient.update_G(G, grad, eta, Gmin, Gmax)
        list_G.append(copy.deepcopy(G))
    number_non_attackers = [0 for v in range(n)]
    for vector in vectors_encountered:
        number_non_attackers[vector] += 1
    optimal_sol = pulp_solve_online(
        number_attackers, CFA, number_non_attackers, gain_success, loss_failure)
    return list_G, vectors_encountered, number_attackers, loss_sum, optimal_sol
