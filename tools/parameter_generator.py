import random


def random_vectors(n, m, low, up, seed):
    random.seed(seed)
    gain_success = []
    loss_failure = []
    CFA = []
    PN = []
    # Choosing a random point on the simplex uniformly
    # First chosing n - 1 points uniformly in an array, adding 0 and 1 to the array and sorting
    helperPN = []
    helperPN.append(0)
    for i in range(n - 1):
        helperPN.append(random.uniform(0, 1))
    helperPN.append(1)
    helperPN = sorted(helperPN)
    # Then, each interval between two points give a coordinate of the n dimensional simplex point
    # This procedure gives a uniform point in the simplex
    for i in range(1, n + 1):
        PN.append(helperPN[i] - helperPN[i - 1])

    for vector in range(n):
        CFA.append(random.uniform(low, up))
    for attackerType in range(m):
        gain_success_vector = []
        loss_failure_vector = []
        for vector in range(n):
            gain_success_vector.append(random.uniform(low, up))
            loss_failure_vector.append(random.uniform(0, up - low))
        gain_success.append(gain_success_vector)
        loss_failure.append(loss_failure_vector)
    return gain_success, loss_failure, CFA, PN


def random_proba_attack(m, seed):
    random.seed(seed)
    probaAttack = []
    sumProba = 0
    for attackerType in range(m):
        proba = random.uniform(0, 1)
        probaAttack.append(proba)
        sumProba += proba
    probaAttack[:] = [proba / sumProba for proba in probaAttack]
    return probaAttack
