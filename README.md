This repository contains the code necessary to run all experiments presented in the paper as well as the data used for the plots.

## Requirements

- Jupyter notebook to run the notebooks
- the content of the file requirements.txt

## Content of the repository

- tools contains helper functions:
	- computation.py contains function to compute the bayesian nash 	equilibrium as well as the gain of players at the equilibrium
	- online_learning.py implements the online gradient descent algorithm
	- gradient.py implements calculus tools
	- parameter_generator.py generates the vectors of the random game
	- write_results.py saves and reads the results of the computation of the equilibrium, the training and the online learning process.
- equilibrium_parameters contains the results of the computation of the equilibrium in the different games studied. 
- online_data contains the results of the online learning algorithm in the different games studied.
- Sample_average_approximation.ipynb contains the code to reproduce experiments on our training algorithm
- Online_learning.ipynb contains the code to reproduce experiments on our online learning algorithm
- Equilibrium illustration contains the code to reproduce illustrations present in appendix.
- Equilibrium_random_compuration contains the code to compute equilibrium of random games (game 3) which are used for our online learning experiments.
- pickle contains pickled data to enable plotting in Sample_average_approximation.ipynb without doing computations
- plots contains the plots presented in the paper



## Reproducing paper results

[Optional] if you do not want to use the data we generated for the plot please delete the content of equilibrium_parameters and online_data. Running Equilibrium_random_computation.ipynb will regenerate equilibrium results. Running Online_learning.py will then automatically regenerate online learning results


- Run Equilibrium_illustrations.ipynb for Figures 2 and 3
- Run Online_learning.ipynb for Figures 8 and 9
- Run Sample_average_approximation.ipynb for all other figures.
